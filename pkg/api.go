package pkg

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"strings"
	"time"
)

type DefaultValue struct {
	timeout time.Duration
}

func SetValue(timeout int64) DefaultValue {
	Value.timeout = time.Second * time.Duration(timeout)

	return Value
}

func GetValue() DefaultValue {
	value := DefaultValue{}

	return value
}

// 初始化一個 struct 型別的變數
var Value = DefaultValue{
	timeout: time.Second * 60, // 預設 60s 過期
}

func Callapi(method, url string, params url.Values) {
	client := http.Client{Timeout: Value.timeout}

	body := strings.NewReader(params.Encode())

	req, err := http.NewRequest(method, url, body)

	CheckError(err)

	req.Header.Set("Accept", `application/json`)

	resp, err := client.Do(req)
	CheckError(err)

	defer resp.Body.Close()

	content, err := ioutil.ReadAll(resp.Body)

	CheckError(err)

	fmt.Println(string(content))
}

func CheckError(err error) {
	if err != nil {
		fmt.Printf("error %s", err)
		return
	}
}
