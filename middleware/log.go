package middleware

import (
	"fmt"
	"os"
	"path"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/sirupsen/logrus"
)

func LoggerToFile() gin.HandlerFunc {
	logger := Logger()
	return func(c *gin.Context) {
		logger.Info(
			logrus.Fields{
				"codeStatus":   c.Writer.Status(),
				"reqMethod":    c.Request.Method,
				"reqURI":       c.Request.RequestURI,
				"request_data": c.Request.PostForm,
				"trace_id":     uuid.New(),
			})
	}
}

func Logger() *logrus.Logger {
	now := time.Now()
	logFilePath := ""

	if dir, err := os.Getwd(); err == nil {
		logFilePath = dir + "/logs/"
	}

	if err := os.MkdirAll(logFilePath, 0777); err != nil {
		fmt.Println(err.Error())
	}

	logFileName := now.Format("2006-01-02") + ".log"
	fileName := path.Join(logFilePath, logFileName)
	if _, err := os.Stat(fileName); err != nil {
		if _, err := os.Create(fileName); err != nil {
			fmt.Println(err.Error())
		}
	}

	src, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, os.ModeAppend)
	if err != nil {
		fmt.Println("err", err)
	}

	logger := logrus.New()

	logger.Out = src

	//輸出設定為標準輸出(預設為stderr)
	logger.SetOutput(os.Stdout)
	//設定要輸出的log等級
	logger.SetLevel(logrus.DebugLevel)
	//log輸出為json格式
	logger.SetFormatter(&logrus.JSONFormatter{
		TimestampFormat: time.RFC3339,
	})

	return logger
}
