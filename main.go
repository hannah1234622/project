package main

import (

	//"cloud-service/app/router"
	//"fmt"
	//"os"
	"go-service/controller"
	"go-service/middleware"

	"github.com/gin-gonic/gin"
)

func main() {
	r := gin.Default()
	r.Use(middleware.LoggerToFile())

	auth := r.Group("/auth")
	{
		auth.POST("/login", controller.Login)
	}

	r.GET("/ping", func(c *gin.Context) {
		c.JSON(200, gin.H{
			"message": "pong",
		})
	})
	r.Run()
	/*

		game := r.Group("/game")
		{
			game.GET("/info", controller.GetGameLost)
		}
		fmt.Println("Start")
		router.MainRouter()
		fmt.Println("Start")
		os.Exit(1)
	*/
}
